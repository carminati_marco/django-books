from django.db import models

# Create your models here.

from django.db import models


class BaseModelManager(models.Manager):
    """
    Base Manager
    """
    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)
    



class BaseModel(models.Model):
    """
    Interface Base
    """    
    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)    
    objects = BaseModelManager()

    def set_deleted(self, *args, **kwargs):
        self.deleted = True
        self.save()
    
    class Meta:
        abstract = True    

        
class Category(BaseModel):
    """
    Category Object. i.e. thriller, fantasy..
    """
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=3)
    class Meta:
        verbose_name_plural = "categories"    
        
    def __str__(self):
        return self.name
        
        
class Author(BaseModel):
    """
    Author
    """
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    
    def __str__(self):
        return "%(first_name)s %(last_name)s" % dict(first_name=self.first_name, last_name=self.last_name)

class PublishingHouse(BaseModel):
    """
    Publishing House
    """
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name    
    
class Book(BaseModel):
    """
    The Book object.
    """
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    publishing_house = models.ForeignKey(PublishingHouse, on_delete=models.SET_NULL, null=True)  # a book can have +Authors
    authors = models.ManyToManyField(Author)

    title = models.CharField(max_length=100)
    description = models.TextField()
    published_at = models.DateField()

    def __str__(self):
        return self.title