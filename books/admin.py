"""
These are the override ModelAdmin.
As I adopted a logical deletion, all ModelAdmin are extended from BaseModelAdmin
"""
from django.contrib import admin
from .models import Author, Book, Category, PublishingHouse


class BaseModelAdmin(admin.ModelAdmin):
    """
    Base Model Admin; used for logical delete.
    """
    def delete_model(BaseModelAdmin, request, obj):
        # call the logical deletion instead of 'delete' method.
        obj.set_deleted()

class CategoryAdmin(BaseModelAdmin):
    search_fields = ('name', 'code')

    fieldsets = [
        ('Category Details', {
            'fields': ['name', 'code']}),
    ]
admin.site.register(Category, CategoryAdmin)        
        
class AuthorAdmin(BaseModelAdmin):
    search_fields = ('first_name', 'last_name')

    fieldsets = [
        ('Author Details', {
            'fields': ['first_name', 'last_name']}),
    ]
admin.site.register(Author, AuthorAdmin)

class PublishingHouseAdmin(BaseModelAdmin):
    search_fields = ('name', )

    fieldsets = [
        ('Publishing House Details', {
            'fields': ['name',]}),
    ]
admin.site.register(PublishingHouse, PublishingHouseAdmin)


class BookAdmin(BaseModelAdmin):
    search_fields = ('title', 'category__name', 'authors__first_name', 'authors__last_name', 'publishing_house__name')
    list_display = ('title', 'category', 'publishing_house')
    list_filter = ('category', 'publishing_house')
    filter_horizontal = ('authors',)
 
    fieldsets = [
        ('Book Details', {
            'fields': ['title', 'description', 'published_at']}),
        ('Book Info', {
            'fields': ['category', 'publishing_house', 'authors']}),            
    ]
admin.site.register(Book, BookAdmin)


