from django.views.generic.list import ListView
from django.utils import timezone

from .models import Category

# I decided to group all books in Category; so I can create a Category's ListView

class CategoryListView(ListView):
    model = Category