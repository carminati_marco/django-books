"""djangobooks URL Configuration
"""
from django.conf.urls import url
from django.contrib import admin
from books.views import CategoryListView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # homepage url.
    url(r'^$', CategoryListView.as_view(), name='category-list'), 

    
]
