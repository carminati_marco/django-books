# README #

This README would normally document whatever steps are necessary to get your application up and running.

## NOTE NOTE NOTE ##
You find a 'db.sqlite3' in the project; 
if you want to create and empty database, remove it and launch the migrate command.

In the example database you can use the superuser
bookstore (password: bookstore)

### What is this repository for? ###

* Quick summary
3 tables
1 OnetoMany relationship
1 ManyToMany relationship
Django Admin for the 3 tables
1 custom view (home page) that displays the data in the database


Due the requests, the tables are

Category
PublishingHouse
Author
Book (with “category” foreign key ()=> 1 OneToMany relationship  and “authors” manytomany)
 The view could be a list view about Category, in order to see the books group by this field.

## How to install it? ##


### Using Docker ###

```
docker build -t books-backend .
docker run -d -p 8000:8000 books-backend
```

### Without Docker ###


Create a new Virtualenv, Python 3 [(install virtualenv)](https://virtualenv.pypa.io/en/stable/installation/).

Activate it, install requirements and migrate the db

```
pip install -r requirements.txt
python manage.py migrate
```

and run server web
```
python manage.py runserver 0.0.0.0:8000
```

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact